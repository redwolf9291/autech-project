<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <title>Interventi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
        }
        th, td {
            text-align: center;
            padding: 16px;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        input[type=text] {
            width:100%;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            background-image: url('http://localhost/autech-project/images/search_icon.png');
            background-size: 2%;
            background-position: 10px 10px; 
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }
    </style>

    <script>
        function link(id, codcli){
            window.location.href = 'http://localhost/autech-project/contratti.php/?id=' + id + "&codcli=" + codcli;
        }

        function SeachBarOurs() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }          
            }
        }
    </script>
</head>
<body>
    <?php
        $vuoto = false;
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "autech";
        $dsn = "mysql:host=$servername;dbname=$dbname;";
        $query1 = ""; $query2 = "";

        $query1 = "SELECT clienti.CodiceInterno, clienti.Nome, clienti.IndirizzoDiFatturazione
                    FROM clienti
                    WHERE clienti.CodiceInterno = '{$_GET['codcli']}'";

        if (isset($_GET["id"])){
            $query2 = "SELECT interventi_help_desk.*, articoli.Descrizione, articoli.CodiceInterno AS 'CodArt', contratti_help_desk.CodiceInterno AS 'CodCon',
                        clienti.CodiceInterno, clienti.Nome, clienti.IndirizzoDiFatturazione
                        FROM contratti_help_desk
                        INNER JOIN interventi_help_desk ON interventi_help_desk.IdContratto = contratti_help_desk.ID
                        INNER JOIN articoli ON articoli.ID = contratti_help_desk.IdArticolo
                        INNER JOIN clienti ON clienti.ID = contratti_help_desk.IdCliente
                        WHERE interventi_help_desk.IdContratto = '{$_GET['id']}'";
        }
        try {
            // connessione al database
            $conn = new PDO($dsn, $username, $password);
            // creazione ed esecuzione della query 1
            $stmt = $conn->prepare($query1);
            $stmt->bindParam(":user",$user);
            $stmt->bindParam(":pass",$pass);
            $stmt->execute();
            // il risultato viene trasformato in array associativo
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // creazione ed esecuzione della query 2
            $stmt = $conn->prepare($query2);
            $stmt->bindParam(":user",$user);
            $stmt->bindParam(":pass",$pass);
            $stmt->execute();
            // il risultato viene trasformato in array associativo
            $result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($result2) == 0){
                $vuoto = true;
            }
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        echo "<form id='form1'>";
    ?>
    <div class='container-fluid'>
    <div class='row'>
    <div class='col-sm-4' style='background-color:#;'>
        <center>
        <p>
            User: <?php echo "{$result[0]["CodiceInterno"]}<br>
                                {$result[0]["Nome"]}<br>
                                {$result[0]["IndirizzoDiFatturazione"]}"; ?>
        </p>
        </center>
    </div>

    <div class='col-sm-8' style='background-color:#;'>
    <input type="text" id="myInput" onkeyup="SeachBarOurs()" placeholder="Search for HelpDesk...">
        <?php
            if (!$vuoto){
                echo "<div class='table-responsive'>
                    <table id='myTable'>
                        <tdead>
                            <tr>
                                <td>N°</td>
                                <td>Date</td>
                                <td>Contract code</td>
                                <td>Type</td>
                                <td>Article code</td>
                                <td>Utilized minutes</td>
                                <td>Note</td>
                            </tr>
                        </tdead>
                        <tbody>";
                            for ($i = 0; $i < count($result2); $i++){
                                echo "
                                    <tr>
                                    <td><input type='button' class='btn btn-light' value='" . ($i + 1) . "' onclick=\"link('{$result2[$i]['IdContratto']}', '{$result2[0]['CodiceInterno']}')\"/></td>
                                    <td>{$result2[$i]['Data']}</td>
                                    <td>{$result2[$i]['CodCon']}</td>
                                    <td>{$result2[$i]['Descrizione']}</td>
                                    <td>{$result2[$i]['CodArt']}</td>
                                    <td>{$result2[$i]['MinutiConsumati']}</td>
                                    <td>{$result2[$i]['Note']}</td></tr>
                                ";
                            }
                            echo "
                </tbody></table></div>";
            }
            else{
                echo "<h1>Non è stato trovato alcun intevento relativo al contratto selezioneato";
            }
            $conn = null;
        ?>
                </div>
        </div>
    </div>
</form>
</body>
</html>